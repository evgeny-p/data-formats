#include "geometry.h"

#include <stdio.h>


int main()
{
    const char point_type = 1;
    const char line_string_type = 2;

    Point pt = {1, 2};
    LineString ls = {.points = {{1, 2}}, .length = 1};

    FILE* out = fopen("canvas.bin", "wb");

    fputc(point_type, out);
    fwrite(&pt, sizeof(pt), 1, out);

    fputc(line_string_type, out);
    fwrite(&ls.length, sizeof(ls.length), 1, out);
    fwrite(ls.points, sizeof(ls.points[0]), ls.length, out);

    fclose(out);

    return 0;
}
