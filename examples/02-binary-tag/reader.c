#include "geometry.h"

#include <stdio.h>

#define point_type 1
#define line_string_type 2

int main()
{
    //const char point_type = 1;
    //const char line_string_type = 2;

    Point pt;
    LineString ls;

    FILE* in = fopen("canvas.bin", "rb");

    char object_type;

    while (1) {
        object_type = fgetc(in);

        if (feof(in)) break;

        switch (object_type) {
            case point_type:
                fread(&pt, sizeof(pt), 1, in);
                printf("Point(%d %d)\n", pt.x, pt.y);
                break;

            case line_string_type:
                fread(&ls.length, sizeof(ls.length), 1, in);
                fread(ls.points, sizeof(ls.points[0]), ls.length, in);
                printf("LineString(%d)\n", ls.length);
                break;

            // case polygon_type: ...
        }
    }

    printf("sz = %d\n", sizeof(LineString));

    fclose(in);

    return 0;
}
