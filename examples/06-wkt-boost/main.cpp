#include <iostream>

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/linestring.hpp>

int main()
{
    using point_type = boost::geometry::model::d2::point_xy<int>;

    point_type pt;
    boost::geometry::model::linestring<point_type> ls;

    boost::geometry::read_wkt("POINT(1 2)", pt);
    boost::geometry::read_wkt("LINESTRING(0 0, 2 2, 3 1)", ls);

    std::cout << pt.x() << " " << pt.y() << std::endl;

    for (const auto& point : ls) {
        std::cout << point.x() << " " << point.y() << std::endl;
    }

    return 0;
}
