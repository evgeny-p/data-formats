#include <libxml/xmlschemastypes.h>

int main()
{
    xmlDocPtr doc;

    xmlSchemaPtr schema;
    xmlSchemaParserCtxtPtr ctxt;
    const char *xml_file_name = "geom.xml";
    const char *xsd_file_name = "geometry.xsd";

    ctxt = xmlSchemaNewParserCtxt(xsd_file_name);

    xmlSchemaSetParserErrors(ctxt, (xmlSchemaValidityErrorFunc) fprintf, (xmlSchemaValidityWarningFunc) fprintf, stderr);

    schema = xmlSchemaParse(ctxt);
    xmlSchemaFreeParserCtxt(ctxt);

    doc = xmlReadFile(xml_file_name, NULL, 0);

    ctxt = xmlSchemaNewValidCtxt(schema);
    xmlSchemaSetValidErrors(ctxt, (xmlSchemaValidityErrorFunc) fprintf, (xmlSchemaValidityWarningFunc) fprintf, stderr);
    int ret = xmlSchemaValidateDoc(ctxt, doc);
    if (ret == 0)
    {
        printf("%s validates\n", xml_file_name);
    }
    else if (ret > 0)
    {
        printf("%s fails to validate\n", xml_file_name);
    }
    else
    {
        printf("%s validation generated an internal error\n", xml_file_name);
    }
    xmlSchemaFreeValidCtxt(ctxt);
    xmlFreeDoc(doc);
    return 0;
}
