#include <libxml/xmlschemastypes.h>
#include <string.h>

int main()
{
    xmlDocPtr doc;
    const char *xml_file_name = "geom.xml";

    doc = xmlReadFile(xml_file_name, NULL, 0);

    xmlNode *root = xmlDocGetRootElement(doc);
    xmlNode *cur_node;
    for (cur_node = root->children; cur_node; cur_node = cur_node->next) {
        if (cur_node->type != XML_ELEMENT_NODE) continue;

        printf("node type: Element, name: %s\n", cur_node->name);
        if (xmlStrcmp(cur_node->name, (const xmlChar*)"point") == 0) {
            xmlChar* x = xmlGetProp(cur_node, "x");
            xmlChar* y = xmlGetProp(cur_node, "y");
            printf("POINT(%s %s)\n", x, y);
        } else if (xmlStrcmp(cur_node->name, (const xmlChar*)"linestring") == 0) {
            // ...
        }
    }

    xmlFreeDoc(doc);
    return 0;
}
