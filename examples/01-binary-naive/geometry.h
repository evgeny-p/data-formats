#pragma once

enum {
    MAX_BUFFER_SIZE = 128
};

typedef struct {
    int x;
    int y;
} Point;

typedef struct {
    Point points[MAX_BUFFER_SIZE];
    int length;
} LineString;

typedef struct {
    LineString lines[MAX_BUFFER_SIZE];
    int length;
} Polygon;
