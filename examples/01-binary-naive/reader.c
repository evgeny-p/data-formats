#include "geometry.h"

#include <stdio.h>

int main()
{
    Point p1;
    Point p2;
    Point p3;

    FILE* in = fopen("canvas.bin", "rb");

    fread(&p1, sizeof(p1), 1, in);
    fread(&p2, sizeof(p2), 1, in);
    fread(&p3, sizeof(p3), 1, in);

    fclose(in);

    printf("POINT(%d %d)\n", p1.x, p1.y);
    printf("POINT(%d %d)\n", p2.x, p2.y);
    printf("POINT(%d %d)\n", p3.x, p3.y);

    return 0;
}
