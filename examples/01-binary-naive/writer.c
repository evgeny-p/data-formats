#include "geometry.h"

#include <stdio.h>

int main()
{
    Point p1 = {1, 2};
    Point p2 = {3, 4};
    Point p3 = {10, 30};

    FILE* out = fopen("canvas.bin", "wb");

    fwrite(&p1, sizeof(p1), 1, out);
    fwrite(&p2, sizeof(p2), 1, out);
    fwrite(&p3, sizeof(p3), 1, out);

    fclose(out);

    return 0;
}
