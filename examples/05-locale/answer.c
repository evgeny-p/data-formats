#include <locale.h>
#include <stdio.h>

int main()
{
    setlocale(LC_ALL, "");
    printf("%.1lf\n", 42.0);
    double a;
    int r = scanf("%lf", &a);
    printf("Read: %d\n", r);
    printf("a = %lf\n", a);
    return 0;
}
